<?php get_header(); ?>

<div id="content" role="main">
  <div class="row">
    <div class="col-12">
      <div class="banner">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <?php $option = get_option("product_page_banner"); ?>
            <?php if(!empty($option)){ ?>
              <?php for ($i=0; $i < count($option); $i++) { ?>
                <?php if ($i==0){ ?>
                  <div class="carousel-item active">
                    <div style="display: block;">
                    <a href="<?php echo $option[$i]["link"]; ?>"><img class="d-block img-fluid" src="<?php echo $option[$i]["image"]; ?>" alt="<?php echo $option[$i]["alt"]; ?>"></a>
                    </div>
                  </div>
                <?php }else{ ?>
                  <div class="carousel-item">
                    <div style="display: block;">
                    <img class="d-block img-fluid" src="<?php echo $option[$i]["image"]; ?>" alt="<?php echo $option[$i]["alt"]; ?>">
                    </div>
                  </div>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          </div>
          <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-10 mx-auto">
      <div class="clearfix" style="height: 2.33rem;"></div>
      <div class="text-center">
        <img src="<?php echo THEME_PATH."/theme/images/all-products-text.jpg"; ?>" style="max-width: 100%;">
      </div>
      <hr class="blue background-blue border-blue">
      <div class="clearfix"></div>
      <div class="text-center bold">
        <?php 
          $parentID = get_category_by_slug( "products" )->term_id;
          $args = array('child_of' => $parentID);
          $categories = get_categories( $args );
          $counter = 0;
          $numItems = count($categories);
        ?>
        <?php foreach ($categories as $category): ?>
          <?php if (++$counter === $numItems ): ?>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo($category->name); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;</span>
          <?php else: ?>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo($category->name); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;|</span>
          <?php endif ?>
        <?php endforeach ?>
      </div>
      <div class="clearfix"></div>
      <?php 
      $args = array( 'numberposts' => -1, 'post_type' => 'products' ); 
      $products = get_posts($args);
      ?>
      <div class="row">
        <?php foreach ($products as $product): ?>
          <div class="col-lg-4">
            <div class="clearfix-15"></div>
            <div class="single-post">
              <div class="wrapper">
                <div class="image">
                  <a href="<?php echo get_permalink($product); ?>"><?php echo get_the_post_thumbnail($product, "full"); ?></a>
                </div>
                <div class="clearfix-15"></div>
                <div class="title text-center">
                  <p><a href="<?php echo get_permalink($product); ?>"><?php echo $product->post_title; ?></a></p>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery('.carousel').carousel();
});
</script>
<?php get_footer(); ?>