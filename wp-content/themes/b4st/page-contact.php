<?php get_header(); ?>

<div id="content" role="main">
  <div class="row">
    <div class="col-12">
      <div class="banner">
        <div class="image">
          <?php $banner = get_option('contact_page_banner'); ?>
          <?php if (!empty($banner)): ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['alt'] ?>">
          <?php else: ?>
            <img src="<?php echo THEME_PATH."/theme/images/about-banner.jpg"; ?>">
          <?php endif ?>
        </div>
        <div class="wrapper">
          <h1 class="text-center">联系我们</h1>
        </div>
      </div>
    </div>
  </div> <!-- end row -->
  <div class="row">
    <div class="col-10 offset-1">
      <div class="clearfix" style="height: 2.33rem;"></div>
      <div class="text-center font-17">
        <h4 class="bold">Orbis Australasia PTY LTD</h4>
        <div class="clearfix-15"></div>
        <div class="location">
          <img src="<?php echo THEME_PATH."/theme/images/location.png"; ?>">
        </div>
        <div class="clearfix-15"></div>
        <p>Suite3 /16 Lexington Dr Bella Vista <br>NSW 2153 Australia</p>

        <div class="location">
          <img src="<?php echo THEME_PATH."/theme/images/phone.png"; ?>">
          &nbsp;&nbsp;&nbsp;&nbsp;
          <img src="<?php echo THEME_PATH."/theme/images/email.png"; ?>">
        </div>

        <div class="clearfix-15"></div>

        <p>Phone: +61 2 8883 0011 <br>Email: info@orbis.net.au</p>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-lg-3 mx-auto text-center">      
          <div class="contact">
            <?php echo do_shortcode('[contact-form-7 id="78" title="Contact form 1"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- end row -->
  <div class="row">
    <div class="col-12">
      <div id="map" style="height: 19.71rem;"></div>
      <script>
        function initMap() {
          var myLatLng = {lat: -33.7366583, lng: 150.9441295};
          var style = 
          [
          {
            elementType: 'geometry',
            stylers: [{color: '#f5f5f5'}]
          },
          {
            elementType: 'labels.icon',
            stylers: [{visibility: 'off'}]
          },
          {
            elementType: 'labels.text.fill',
            stylers: [{color: '#616161'}]
          },
          {
            elementType: 'labels.text.stroke',
            stylers: [{color: '#f5f5f5'}]
          },
          {
            featureType: 'administrative.land_parcel',
            elementType: 'labels.text.fill',
            stylers: [{color: '#bdbdbd'}]
          },
          {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [{color: '#eeeeee'}]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#757575'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#e5e5e5'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#ffffff'}]
          },
          {
            featureType: 'road.arterial',
            elementType: 'labels.text.fill',
            stylers: [{color: '#757575'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#dadada'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#616161'}]
          },
          {
            featureType: 'road.local',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
          },
          {
            featureType: 'transit.line',
            elementType: 'geometry',
            stylers: [{color: '#e5e5e5'}]
          },
          {
            featureType: 'transit.station',
            elementType: 'geometry',
            stylers: [{color: '#eeeeee'}]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#c9c9c9'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
          }
          ];

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: myLatLng,
            disableDefaultUI: true,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
          });
          map.setOptions({styles: style});

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
          });
        }
      </script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMwzNak7PXmHgJOGPSVwSlr50N6WK1Cvg&callback=initMap"></script>
    </div> <!-- end col -->
  </div> <!-- end row -->
</div>

<?php get_footer(); ?>