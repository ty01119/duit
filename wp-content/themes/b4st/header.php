<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">
    var isOpen = false;
    $(document).ready(function () {
      $('.navbar-toggler').on('click', function() {
        if (!isOpen) {
          $(this).find("img").attr("src","<?php echo THEME_PATH . "/theme/images/menu-open.png" ; ?>");
          isOpen = true;
        }
        else{
          $(this).find("img").attr("src","<?php echo THEME_PATH . "/theme/images/menu-close.png" ; ?>");
          isOpen = false;
        }
      });
    });
  </script>
</head>

<body <?php body_class(); ?>>
<div class="navi fixed-top background-white hidden-md-down">
  <nav class="wrapper">
    <div class="clearfix-25"></div>
    <div class="col-lg-10 offset-lg-1">
      <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php echo THEME_PATH . "/theme/images/logo.png" ; ?>"></a>
      <div class="search">
        <div class="ml-auto wrapper">
          <?php get_template_part('navbar-search'); ?>
          <div class="tags">
            <span class="tag"><a href="<?php echo home_url() ?>/?s=保湿">保湿</a></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="tag"><a href="<?php echo home_url() ?>/?s=护理">护理</a></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="tag"><a href="<?php echo home_url() ?>/?s=锁水">锁水</a></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="tag"><a href="<?php echo home_url() ?>/?s=去角质">去角质</a></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="tag"><a href="<?php echo home_url() ?>/?s=润肤乳">润肤乳</a></span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix-25"></div>
  </nav>
  <nav class="navbar navbar-toggleable-md navbar-light nav-down hidden-sm-down ">
    <div class="col-lg-10 offset-lg-1">
      <div class="ml-auto">
      <?php
        wp_nav_menu( array(
          'theme_location'    => 'navbar',
          'container'         => false,
          'menu_class'        => '',
          'fallback_cb'       => '__return_false',
          'items_wrap'        => '<ul id="%1$s" class="navbar-nav mr-auto mt-2 mt-lg-0 %2$s">%3$s</ul>',
          'depth'             => 2,
          'walker'            => new b4st_walker_nav_menu()
        ) );
      ?>
      </div>
    </div>
  </nav>
</div>

<div class="navi-small fixed-top background-white hidden-lg-up">
  <nav class="navbar navbar-toggleable-md">
    <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php echo THEME_PATH . "/theme/images/logo.png" ; ?>"></a>
    <div class="search">
      <div class="wrapper">
        <?php get_template_part('navbar-search'); ?>
      </div>
    </div>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><img src="<?php echo THEME_PATH . "/theme/images/menu-close.png" ; ?>" style="width: 100%; max-width: 100%;"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <?php
        wp_nav_menu( array(
          'menu'              => 'menu_mobile',
          'theme_location'    => 'navbar',
          'container'         => false,
          'menu_class'        => '',
          'fallback_cb'       => '__return_false',
          'items_wrap'        => '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
          'depth'             => 2,
          'walker'            => new b4st_walker_nav_menu()
        ) );
      ?>
  </nav>
</div>



