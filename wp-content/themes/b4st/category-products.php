<?php get_header(); ?>

<div id="content" role="main">
  <div id="category">
    <div class="row">
      <div class="col-12">
        <div class="banner">
          <div class="image">
            <img src="<?php echo THEME_PATH."/theme/images/products-banner-1.jpg"; ?>">
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-10 mx-auto">
        <div class="breadcrumb">
          <span><a href="<?php echo home_url(); ?>">首页</a></span>&nbsp;&nbsp;&gt;
          <span><a href="<?php echo home_url(); ?>/products">全线产品</a></span>&nbsp;&nbsp;&gt;
          <span><a href="#"><?php echo single_cat_title(); ?></a></span>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-lg-2 sidebar hidden-md-down">
            <?php 
            $index = 0;
            $current = 0;
            $productsCategory = get_category_by_slug("products");
            $categories=get_categories(
                array( 'parent' => $productsCategory->term_id )
            );
            // var_dump($categories);
            ?>
            <!-- accordion begin -->
            <div id="accordion">
              <?php 
              foreach ($categories as $category){
                if ($category->name == single_cat_title('', false)) {
                  $current = $index;
                }
                $index++;
                echo "<h2>$category->name</h2>";
                echo  "<div>" ;
                $products = get_posts(
                  array(
                    'numberposts' => -1, 
                    'category' => $category->term_id,
                    'post_type' => "products",
                  )
                );
                foreach ($products as $product) {
                  echo "<p class=\"font-18\"><a href=\"" . get_permalink($product). "\">$product->post_title</a></p>";
                }
                echo "</div>";
              }
              ?>
            </div> <!-- accordion end -->
            <div class="clearfix"></div>
            <?php 
            $recommendedProducts = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'products',
                'meta_key' => 'product_recommended',
                'meta_value' => true
              ));
            ?>
            <div class="recommended-img text-center">
              <img src="<?php echo THEME_PATH ?>/theme/images/recommended.png">
            </div>
            <div class="clearfix-15"></div>
            <div id="recommended">
              <div class="recommended">
                <ul style="margin: 0px; padding: 0px; position: relative; list-style: none;">
                  <?php foreach ($recommendedProducts as $recommended): ?>
                    <li class="single-recommended">
                      <div class="clearfix-15"></div>
                      <div class="image">
                        <a href="<?php echo get_permalink($recommended); ?>"><?php echo get_the_post_thumbnail($recommended, "full"); ?></a>
                      </div>
                      <div class="clearfix-10"></div>
                      <div class="title text-center"><a href="<?php echo get_permalink($recommended); ?>"><?php echo $recommended->post_title; ?></a></div>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
              <div class="navi font-30 text-center">
                <a href="#" class="prev"><i class="fa fa-caret-up blue"></i></a>
                &nbsp;&nbsp;&nbsp;
                <a href="#" class="next"><i class="fa fa-caret-down blue"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-10">
            <div style="margin-top: 1rem;"></div>
            <div class="product-category">
              <h2 class="blue"><?php echo single_cat_title(); ?></h2>
              <hr class="border-blue">
            </div>
            <div class="all-products">
              <div class="row">
                <?php 
                $obj = get_queried_object();
                // var_dump($obj);
                $products = get_posts(
                  array(
                    'numberposts' => -1, 
                    'category' => $obj->term_id,
                    'post_type' => "products",
                  )
                );
                ?>
                <?php foreach ($products as $product): ?>
                  <div class="col-lg-4">
                    <div class="clearfix-10"></div>
                    <div class="single-product">
                      <div class="image"><a href="<?php echo get_permalink($product); ?>"><?php echo get_the_post_thumbnail($product, 'full'); ?></a></div>
                      <div class="clearfix-10"></div>
                      <div class="title text-center"><a href="<?php echo get_permalink($product); ?>"><h5><?php echo $product->post_title; ?></h5></a></div>
                    </div>
                  </div>
                <?php endforeach ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<script type="text/javascript">
  $(function() {
    var icons = {
      header: "fa fa-caret-right",
      activeHeader: "fa fa-caret-down"
    };
    $( "#accordion" ).accordion({
      collapsible: true,
      icons: icons,
      heightStyle: "content",
      active: <?php echo $current ?>,
    });
  });
$(window).load(function() {
  jQuery(document).ready(function(){
    jQuery("#recommended .recommended").jCarouselLite({
      btnNext: "#recommended .next",
      btnPrev: "#recommended .prev",
      vertical: true
    });
  });
});
  
</script>
<?php get_footer(); ?>