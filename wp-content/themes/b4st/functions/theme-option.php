<?php 
function theme_settings_page()
{
    ?>
	    <div class="wrap">
	    <h1>Theme Panel</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}
function add_theme_menu_item()
{
	add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function display_home_page_banner()
{
  ?>
    Banner 1 Image URL: <input type="text" name="home_page_banner[0][image]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[0]["image"]; ?>" />
    Banner 1 Image ALT: <input type="text" name="home_page_banner[0][alt]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[0]["alt"]; ?>" />
    Banner 1 Image Link: <input type="text" name="home_page_banner[0][link]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[0]["link"]; ?>" />
    <br>
    Banner 2 Image URL: <input type="text" name="home_page_banner[1][image]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[1]["image"]; ?>" />
    Banner 2 Image ALT: <input type="text" name="home_page_banner[1][alt]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[1]["alt"]; ?>" />
    Banner 2 Image Link: <input type="text" name="home_page_banner[1][link]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[1]["link"]; ?>" />
    <br>
    Banner 3 Image URL: <input type="text" name="home_page_banner[2][image]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[2]["image"]; ?>" />
    Banner 3 Image ALT: <input type="text" name="home_page_banner[2][alt]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[2]["alt"]; ?>" />
    Banner 3 Image Link: <input type="text" name="home_page_banner[2][link]" id="home_page_banner" value="<?php echo get_option('home_page_banner')[2]["link"]; ?>" />
  <?php
}

function display_about_page_banner()
{
  ?>
    Banner Image URL: <input type="text" name="about_page_banner[image]" id="about_page_banner" value="<?php echo get_option('about_page_banner')["image"]; ?>" />
    Banner Image ALT: <input type="text" name="about_page_banner[alt]" id="about_page_banner" value="<?php echo get_option('about_page_banner')["alt"]; ?>" />
  <?php
}

function display_wheretobuy_page_banner()
{
  ?>
    Banner Image URL: <input type="text" name="wheretobuy_page_banner[image]" id="wheretobuy_page_banner" value="<?php echo get_option('wheretobuy_page_banner')["image"]; ?>" />
    Banner Image ALT: <input type="text" name="wheretobuy_page_banner[alt]" id="wheretobuy_page_banner" value="<?php echo get_option('wheretobuy_page_banner')["alt"]; ?>" />
  <?php
}

function display_contact_page_banner()
{
  ?>
    Banner Image URL: <input type="text" name="contact_page_banner[image]" id="contact_page_banner" value="<?php echo get_option('contact_page_banner')["image"]; ?>" />
    Banner Image ALT: <input type="text" name="contact_page_banner[alt]" id="contact_page_banner" value="<?php echo get_option('contact_page_banner')["alt"]; ?>" />
  <?php
}


function display_product_page_banner()
{
  ?>
    Banner 1 Image URL: <input type="text" name="product_page_banner[0][image]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[0]["image"]; ?>" />
    Banner 1 Image ALT: <input type="text" name="product_page_banner[0][alt]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[0]["alt"]; ?>" />
    Banner 1 Image Link: <input type="text" name="product_page_banner[0][link]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[0]["link"]; ?>" />
    <br>
    Banner 2 Image URL: <input type="text" name="product_page_banner[1][image]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[1]["image"]; ?>" />
    Banner 2 Image ALT: <input type="text" name="product_page_banner[1][alt]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[1]["alt"]; ?>" />
    Banner 2 Image Link: <input type="text" name="product_page_banner[1][link]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[1]["link"]; ?>" />
    <br>
    Banner 3 Image URL: <input type="text" name="product_page_banner[2][image]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[2]["image"]; ?>" />
    Banner 3 Image ALT: <input type="text" name="product_page_banner[2][alt]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[2]["alt"]; ?>" />
    Banner 3 Image Link: <input type="text" name="product_page_banner[2][link]" id="product_page_banner" value="<?php echo get_option('product_page_banner')[2]["link"]; ?>" />
  <?php
}

function display_theme_panel_fields()
{

  add_settings_field("home_page_banner", "Home Page Banner", "display_home_page_banner", "theme-options", "section");

  register_setting("section", "home_page_banner");

  add_settings_field("about_page_banner", "About Page Banner", "display_about_page_banner", "theme-options", "section");

  register_setting("section", "about_page_banner");

  add_settings_field("wheretobuy_page_banner", "WheretoBuy Page Banner", "display_wheretobuy_page_banner", "theme-options", "section");

  register_setting("section", "wheretobuy_page_banner");

  add_settings_field("contact_page_banner", "Contact Page Banner", "display_contact_page_banner", "theme-options", "section");

  register_setting("section", "contact_page_banner");

  add_settings_field("product_page_banner", "Product Page Banner", "display_product_page_banner", "theme-options", "section");

  register_setting("section", "product_page_banner");

  add_settings_section("section", "All Settings", null, "theme-options");
}

add_action("admin_init", "display_theme_panel_fields");

