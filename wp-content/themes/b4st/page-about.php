<?php get_header(); ?>

<div id="content" role="main">
  <div id="about">
    <div class="row">
      <div class="col-12">
        <div class="banner">
          <div class="image">
            <?php $banner = get_option('about_page_banner'); ?>
            <?php if (!empty($banner)): ?>
              <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['alt'] ?>">
            <?php else: ?>
              <img src="<?php echo THEME_PATH."/theme/images/about-banner.jpg"; ?>">
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10 mx-auto">
        <div class="title">
          <div class="wrapper text-center blue">
            <div class="inner">
              <br>
              <h1>关于DU’IT</h1>
              <h5>About DU’IT</h5>
              <br>
            </div>
          </div>
        </div>
        <div class="text-center">
          DU’IT 创立于1998年，由护理医师Zina和丈夫Pynith共同创办，拥有手部、足部、
          <br>
          面部及婴儿护理产品。护肤理念注重天然与功效，不仅在澳洲家喻户晓，产品已
          <br>
          销往美英中等8个国家。DU’IT成功的关键因素源自科学独有配方，适用于干燥和
          <br>
          敏感性肌肤。其明星产品急救手膜护肤效果5日可见，2016年单只销量达到180万
          <br>
          支，成为澳洲皮肤科医师推荐功效产品。
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row no-gutters">
          <div class="col-lg-6">
            <div class="image">
              <img src="<?php echo THEME_PATH; ?>/theme/images/about-1.jpg">
            </div>
          </div>
          <div class="col-lg-6 section" style="text-align: left;">
            <div>
              <div class="clearfix"></div>
              <h2 class="blue text-center">注重细节 始于足下</h2>
              <hr class="text-center">
              <p>1988年，在医疗行业工作的Zina，发现她的患者们常常来咨询一
              <br>个看似微小却困扰着很多人的疑难杂症—足部干裂及皮肤干燥问<br>题。</p>
              <p>起始于此，Zina开始专研配方，试图找到一款安全特效的护足霜</p>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-lg-6 push-lg-6">
            <div class="image">
              <img src="<?php echo THEME_PATH; ?>/theme/images/about-2.jpg">
            </div>
          </div>
          <div class="col-lg-6 pull-lg-6 section" style="text-align: left;">
            <div>
              <div class="clearfix"></div>
              <h2 class="blue text-center">它成为了澳洲足部医师的共同选择</h2>
              <hr class="text-center">
              <p>经过上千次的实验与配方调整，Zina在药剂师和业内护肤品专家的帮<br>助下，终于研制出首款功效与天然兼备的产品 — DU’IT的经典畅销产品<br>足部护理膏。
              </p>
              <p>它不仅可以帮助受到足部皮肤干燥问题困扰的普通人，还可以针对糖尿<br>
              病足保养和修护，大大改善了他们的生活质量。产品收获了大量足部医<br>
              师和使用者的积极反馈，使夫妇俩更加坚定自己的护肤理念。
              </p>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-12">
            <div class="image">
              <img src="<?php echo THEME_PATH; ?>/theme/images/about-3.jpg">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="image">
              <img src="<?php echo THEME_PATH; ?>/theme/images/about-4.jpg">
            </div>
          </div>
          <div class="col-lg-6 section text-center">
            <div>
              <div class="clearfix"></div>
              <h2 class="blue">澳洲功效护肤口碑品牌</h2>
              <hr>
              <p>DU’IT恪守产品质量为先的原则，有序的增加手部、足部及婴儿等系列产品线。总是保<br>持一贯的理念：</p>
              <p><span class="blue">独立研发团队</span><br>20年经验生化科学团队不断思考产品理念，根据用户需求持续升华配方</p>
              <p><span class="blue">最有信誉的供应商</span><br>精选澳洲得天独厚的天然原料，将现代技术与长期以来的科学实践相结合</p>
              <p><span class="blue">最严谨的态度</span><br>细致的市场分析，深入了解真正的护肤需求，通过澳大利亚最严苛的制造标准审核， <br>获得最高质量和效能的配方</p>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-12 blue section">
            <div class="wrapper">
            <h1>“ 研发和制造高性价比护肤品，<br>
                以疗效和质量为准则，<br>
                以改善健康为目标。 <br>
                我们不随波逐流，我们引领潮流。”
            </h1>
            <p class="text-right">——创始人Zina Richter</p>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
      </div>
    </div>
    <?php $posts = get_posts(array("numberposts" => -1, "post_type" => "timeline", "order"=>"ASC", "order_by" => "title")); ?>
    <div class="timeline-background hidden-md-down">
      <div id="timeline">
        <div class="dot"></div>
        <div class="time-block">
          <div class="mask-left"></div>
          <div class="clearfix"></div>
          <div class="clearfix"></div>
          <ul id="dates">
            <?php foreach ($posts as $post): ?>
              <li><a href="#<?php echo get_the_title($post); ?>"><?php echo get_the_title($post); ?></a></li>
            <?php endforeach ?>
            <div class="clearfix"></div>
          </ul>
          <div class="mask-right"></div>
        </div>
        <div class="dot-blue"></div>
        <ul id="issues">
          <?php foreach ($posts as $post): ?>
            <li id="<?php echo get_the_title($post); ?>">
              <div class="wrapper">
                <div class="images">
                  <div class="image-wrapper">
                    <div class="image-left">
                      <?php echo wp_get_attachment_image(get_field("timeline_left_image", $post)) ?>
                    </div>
                    <div class="image-right">
                      <?php echo wp_get_attachment_image(get_field("timeline_right_image", $post)); ?>
                    </div>
                  </div>
                </div>
                <div class="content">
                  <div class="content-wrapper">
                    <?php echo get_field("time_content", $post); ?>
                  </div>
                </div>
              </div>
            </li>
          <?php endforeach ?>
        </ul>
        <a href="#" id="next">+</a>
        <a href="#" id="prev">-</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  jQuery(function(){
   jQuery().timelinr({
      orientation: 'horizontal',
      // value: horizontal | vertical, default to horizontal
      containerDiv: '#timeline',
      // value: any HTML tag or #id, default to #timeline
      datesDiv: '#dates',
      // value: any HTML tag or #id, default to #dates
      datesSelectedClass: 'selected',
      // value: any class, default to selected
      datesSpeed: 'normal',
      // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to normal
      issuesDiv : '#issues',
      // value: any HTML tag or #id, default to #issues
      issuesSelectedClass: 'selected',
      // value: any class, default to selected
      issuesSpeed: 'fast',
      // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to fast
      issuesTransparency: 0.2,
      // value: integer between 0 and 1 (recommended), default to 0.2
      issuesTransparencySpeed: 500,
      // value: integer between 100 and 1000 (recommended), default to 500 (normal)
      prevButton: '#prev',
      // value: any HTML tag or #id, default to #prev
      nextButton: '#next',
      // value: any HTML tag or #id, default to #next
      arrowKeys: 'false',
      // value: true/false, default to false
      startAt: 1,
      // value: integer, default to 1 (first)
      autoPlay: 'true',
      // value: true | false, default to false
      autoPlayDirection: 'forward',
      // value: forward | backward, default to forward
      autoPlayPause: 8000
      // value: integer (1000 = 1 seg), default to 2000 (2segs)< });
   });
 });
</script>
<?php get_footer(); ?>