<?php get_header(); ?>

<div id="content" role="main">
  <div class="row news-list">
    <div class="col-10 offset-1">
      <div class="clearfix" style="height: 2.33rem;"></div>
      <!-- Tab container -->
      <div class="tab-container">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <?php if (!isset($_GET['page']) || $_GET['page']=="skincare"): ?>
            <a class="nav-link active" href="#skincare" role="tab" data-toggle="tab">护肤小贴士</a>
          <?php else: ?>
            <a class="nav-link" href="#skincare" role="tab" data-toggle="tab">护肤小贴士</a>
          <?php endif ?>
        </li>
        <li class="nav-item">
          <?php if ($_GET['page']=="childcare"): ?>
            <a class="nav-link active" href="#childcare" role="tab" data-toggle="tab">母婴育儿</a>
          <?php else: ?>
            <a class="nav-link" href="#childcare" role="tab" data-toggle="tab">母婴育儿</a>
          <?php endif ?>
        </li>
        <li class="nav-item">
          <?php if ($_GET['page']=="advise"): ?>
            <a class="nav-link active" href="#advise" role="tab" data-toggle="tab">专家建议</a>
          <?php else: ?>
            <a class="nav-link" href="#advise" role="tab" data-toggle="tab">专家建议</a>
          <?php endif ?>
        </li>

        <li class="nav-item">
          <?php if ($_GET['page']=="latest"): ?>
            <a class="nav-link active" href="#latest" role="tab" data-toggle="tab">最新动态</a>
          <?php else: ?>
            <a class="nav-link" href="#latest" role="tab" data-toggle="tab">最新动态</a>
          <?php endif ?>
        </li>

        <li class="nav-item">
          <?php if ($_GET['page']=="faq"): ?>
            <a class="nav-link active" href="#faq" role="tab" data-toggle="tab">常见问题</a>
          <?php else: ?>
            <a class="nav-link" href="#faq" role="tab" data-toggle="tab">常见问题</a>
          <?php endif ?>
        </li>
      </ul>
      </div> <!-- end tab container -->
      <!-- Tab content -->
      <div class="tab-content">
        <?php if (!isset($_GET['page']) || $_GET['page']=="skincare"): ?>
        <div role="tabpanel" class="tab-pane active" id="skincare">
        <?php else: ?>
        <div role="tabpanel" class="tab-pane" id="skincare">
        <?php endif ?>
          <?php 
          $args = array( 'numberposts' => -1, 'category_name' => 'skincare', 'post_type' => 'post' ); 
          $posts = get_posts($args);
          ?>
          <?php foreach ($posts as $post): ?>
          <div class="post">
            <div class="single-post">
              <div class="row">
                <div class="col-lg-5">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-7">
                  <div class="info">
                    <div class="title">
                      <a href="<?php echo get_permalink($post); ?>"><h2><?php echo $post->post_title; ?></h2></a>
                    </div>
                    <div class="clearfix-15"></div>
                    <div class="date">
                      <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="excerpt">
                      <p><?php echo get_the_excerpt($post); ?></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="tags">
                        <b>标签：</b>
                          <?php 
                          $tags = get_the_tags($post->ID);
                          if ($tags){
                            $index = 0; 
                            $count = count($tags);
                            foreach ($tags as $tag) {
                              $index ++;
                              if ($index < $count) {
                                echo "<b>$tag->name, </b>";
                              }
                              else{
                                echo "<b>$tag->name</b>";
                              }
                            }
                          } 
                          ?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="category">
                        <b>分类：</b>
                          <?php 
                          $categories = get_the_category($post->ID);
                          $index = 0;
                          $count = count($categories);
                          if ($categories) {
                            foreach ($categories as $category) {
                              if ($category->category_parent==9) {
                                echo "<b>$category->name</b>";
                              }
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="readmore">
                      <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        <?php if ($_GET['page']=="childcare"): ?>
        <div role="tabpanel" class="tab-pane active" id="childcare">
        <?php else: ?>
        <div role="tabpanel" class="tab-pane" id="childcare">
        <?php endif ?>
          <?php 
          $args = array( 'numberposts' => -1, 'category_name' => 'childcare', 'post_type' => 'post' ); 
          $posts = get_posts($args);
          ?>
          <?php foreach ($posts as $post): ?>
          <div class="post">
            <div class="single-post">
              <div class="row">
                <div class="col-lg-5">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-7">
                  <div class="info">
                    <div class="title">
                      <h2><?php echo $post->post_title; ?></h2>
                    </div>
                    <div class="clearfix-15"></div>
                    <div class="date">
                      <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="excerpt">
                      <p><?php echo get_the_excerpt($post); ?></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="tags">
                        <b>标签：</b>
                          <?php 
                          $tags = get_the_tags($post->ID);
                          if ($tags){
                            $index = 0; 
                            $count = count($tags);
                            foreach ($tags as $tag) {
                              $index ++;
                              if ($index < $count) {
                                echo "<b>$tag->name, </b>";
                              }
                              else{
                                echo "<b>$tag->name</b>";
                              }
                            }
                          } 
                          ?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="category">
                        <b>分类：</b>
                          <?php 
                          $categories = get_the_category($post->ID);
                          $index = 0;
                          $count = count($categories);
                          if ($categories) {
                            foreach ($categories as $category) {
                              if ($category->category_parent==10) {
                                echo "<b>$category->name</b>";
                              }
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="readmore">
                      <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        <?php if ($_GET['page']=="advise"): ?>
        <div role="tabpanel" class="tab-pane active" id="advise">
        <?php else: ?>
        <div role="tabpanel" class="tab-pane" id="advise">
        <?php endif ?>
          <?php 
          $args = array( 'numberposts' => -1, 'category_name' => 'advise', 'post_type' => 'post' ); 
          $posts = get_posts($args);
          ?>
          <?php foreach ($posts as $post): ?>
          <div class="post">
            <div class="single-post">
              <div class="row">
                <div class="col-lg-5">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-7">
                  <div class="info">
                    <div class="title">
                      <h2><?php echo $post->post_title; ?></h2>
                    </div>
                    <div class="clearfix-15"></div>
                    <div class="date">
                      <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="excerpt">
                      <p><?php echo get_the_excerpt($post); ?></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="tags">
                        <b>标签：</b>
                          <?php 
                          $tags = get_the_tags($post->ID);
                          if ($tags){
                            $index = 0; 
                            $count = count($tags);
                            foreach ($tags as $tag) {
                              $index ++;
                              if ($index < $count) {
                                echo "<b>$tag->name, </b>";
                              }
                              else{
                                echo "<b>$tag->name</b>";
                              }
                            }
                          } 
                          ?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="category">
                        <b>分类：</b>
                          <?php 
                          $categories = get_the_category($post->ID);
                          $index = 0;
                          $count = count($categories);
                          if ($categories) {
                            foreach ($categories as $category) {
                              if ($category->category_parent==11) {
                                echo "<b>$category->name</b>";
                              }
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="readmore">
                      <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        <?php if ($_GET['page']=="latest"): ?>
        <div role="tabpanel" class="tab-pane active" id="latest">
        <?php else: ?>
        <div role="tabpanel" class="tab-pane" id="latest">
        <?php endif ?>
          <?php 
          $args = array( 'numberposts' => -1, 'category_name' => 'latest', 'post_type' => 'post' ); 
          $posts = get_posts($args);
          ?>
          <?php foreach ($posts as $post): ?>
          <div class="post">
            <div class="single-post">
              <div class="row">
                <div class="col-lg-5">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-7">
                  <div class="info">
                    <div class="title">
                      <h2><?php echo $post->post_title; ?></h2>
                    </div>
                    <div class="clearfix-15"></div>
                    <div class="date">
                      <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="excerpt">
                      <p><?php echo get_the_excerpt($post); ?></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="tags">
                        <b>标签：</b>
                          <?php 
                          $tags = get_the_tags($post->ID);
                          if ($tags){
                            $index = 0; 
                            $count = count($tags);
                            foreach ($tags as $tag) {
                              $index ++;
                              if ($index < $count) {
                                echo "<b>$tag->name, </b>";
                              }
                              else{
                                echo "<b>$tag->name</b>";
                              }
                            }
                          } 
                          ?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="category">
                        <b>分类：</b>
                          <?php 
                          $categories = get_the_category($post->ID);
                          $index = 0;
                          $count = count($categories);
                          if ($categories) {
                            foreach ($categories as $category) {
                              if ($category->category_parent==12) {
                                echo "<b>$category->name</b>";
                              }
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="readmore">
                      <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        <?php if ($_GET['page']=="faq"): ?>
        <div role="tabpanel" class="tab-pane active" id="faq">
        <?php else: ?>
        <div role="tabpanel" class="tab-pane" id="faq">
        <?php endif ?>
          <?php 
          $args = array( 'numberposts' => -1, 'category_name' => 'faq', 'post_type' => 'post' ); 
          $posts = get_posts($args);
          ?>
          <?php foreach ($posts as $post): ?>
          <div class="post">
            <div class="single-post">
              <div class="row">
                <div class="col-lg-5">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                </div>
                <div class="col-lg-7">
                  <div class="info">
                    <div class="title">
                      <h2><?php echo $post->post_title; ?></h2>
                    </div>
                    <div class="clearfix-15"></div>
                    <div class="date">
                      <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="excerpt">
                      <p><?php echo get_the_excerpt($post); ?></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="tags">
                        <b>标签：</b>
                          <?php 
                          $tags = get_the_tags($post->ID);
                          if ($tags){
                            $index = 0; 
                            $count = count($tags);
                            foreach ($tags as $tag) {
                              $index ++;
                              if ($index < $count) {
                                echo "<b>$tag->name, </b>";
                              }
                              else{
                                echo "<b>$tag->name</b>";
                              }
                            }
                          } 
                          ?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="category">
                        <b>分类：</b>
                          <?php 
                          $categories = get_the_category($post->ID);
                          $index = 0;
                          $count = count($categories);
                          // var_dump($categories);
                          if ($categories) {
                            foreach ($categories as $category) {
                              // var_dump($category);
                              if ($category->category_parent==13) {
                                echo "<b>$category->name</b>";
                              }
                            }
                          }
                          ?>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="readmore">
                      <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        
      </div> <!-- end tab content -->
    </div>
  </div> <!-- end row -->
</div>
<div class="clearfix"></div>
<?php get_footer(); ?>
