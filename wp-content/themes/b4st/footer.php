<footer class="background-blue white hidden-md-down">
<div class="clearfix"></div>
<div class="col-lg-10 offset-lg-1">
  <div class="row">
    <div class="col-5">
      <div class="font-14">
        <a href="<?php echo home_url() ?>/about/">关于DU’IT</a> | <a href="<?php echo home_url() ?>/news/?page=faq">常见问题</a> | <a href="<?php echo home_url() ?>/wheretobuy/">如何购买</a> | <a href="<?php echo home_url() ?>/contact/">联系DU’IT</a>
      </div>
      <div class="clearfix-10"></div>
      <div class="clearfix-10"></div>
      <div class="clearfix-10"></div>
      <div class="clearfix-10"></div>
      <div class="font-10 light">
        <!-- Copyright © 2017 DUIT All Rights Reserved. -->
        Orbis Australasia Pty Ltd.
      </div>
    </div>
    <div class="col">
      <div>
        关注我们
      </div>
      <div class="clearfix-10"></div>

      <div class="social">

        <div class="footer-modal modal-hide" style="position: absolute; display: none;">
          <div class="image">
              <img src="<?php echo THEME_PATH. "/theme/images/footer-QR.png"; ?>" style="width: 150px;">
          </div>
        </div>
        <img src="<?php echo THEME_PATH. "/theme/images/footer-wechat.png"; ?>" class="wechat-icon">
        <a href="http://weibo.com/duitskincare" target="_blank"><img src="<?php echo THEME_PATH. "/theme/images/footer-weibo.png"; ?>"></a>
        <a href="https://www.instagram.com/duitau/" target="_blank"><img src="<?php echo THEME_PATH. "/theme/images/footer-instagram.png"; ?>"></a>
      </div>
      <div class="clearfix-10"></div>
      <div>
        咨询热线：+61 2 8883 0011
      </div>
    </div>
    <div class="col text-center font-8 light">
      <div style="display: none; letter-spacing: 1px;">
        <img src="<?php echo THEME_PATH. "/theme/images/qr1.png"; ?>" style="width:5rem; margin-bottom: 5px;">
        <br>
        DU'IT 海外市场部
      </div>
      <div style="display: inline-block; letter-spacing: 1px;">
        <img src="<?php echo THEME_PATH. "/theme/images/qr2.png"; ?>" style="width:5rem; margin-bottom: 5px;">
        <br>
        DU'IT 官方微信
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
</footer>


<footer class="background-blue white hidden-lg-up footer-small">
  <div class="row border-bottom">
    <div class="col-12 text-center">
      <div class="clearfix-15"></div>
      <div class="font-20">
        <a href="<?php echo home_url() ?>/about/">关于DU’IT</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo home_url() ?>/news/?page=faq">常见问题</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo home_url() ?>/wheretobuy/">如何购买</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?php echo home_url() ?>/contact/">联系DU’IT</a>
      </div>
      <div class="clearfix-15"></div>
    </div>
  </div>
  <div class="row border-bottom">
    <div class="col-10 mx-auto">
      <div class="clearfix-15"></div>
      <div class="row">
        <div class="col-6 text-right border-right">
          <div class="latest-magazine mx-auto">
              <?php $magazine = get_posts(array('numberposts' => 1, 'post_type' => 'magazine')); ?>
              <a href="<?php echo get_permalink($magazine[0]) ?>" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/i1.png" ;?>"></a>
          </div>
        </div>

        <div class="col-6 text-left">
          <div style="display: inline-block;">
            <div>
              关注我们
            </div>
            <div class="clearfix-10"></div>
            <div class="footer-modal-small" style="position: absolute; display: none;">
              <div class="image">
                  <img src="<?php echo THEME_PATH. "/theme/images/footer-QR.png"; ?>" style="width: 100px;">
              </div>
            </div>
            <div class="social">
                <img src="<?php echo THEME_PATH. "/theme/images/footer-wechat.png"; ?>" class="wechat-icon-small">
                <a href="http://weibo.com/duitskincare" target="_blank"><img src="<?php echo THEME_PATH. "/theme/images/footer-weibo.png"; ?>"></a>
                <a href="https://www.instagram.com/duitau/" target="_blank"><img src="<?php echo THEME_PATH. "/theme/images/footer-instagram.png"; ?>"></a>
            </div>
            <div class="clearfix-10"></div>
            <div class="font-12">
              咨询热线：<br>+61 2 8883 0011
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix-15"></div>
    </div>
  </div>
  <div class="row border-bottom">
    <div class="col-10 mx-auto">
      <div class="clearfix-15"></div>
      <div class="row">
<!--        <div class="col-6 text-right">-->
<!--          <div class="text-center font-23" style="display: none;">-->
<!--            <img src="--><?php //echo THEME_PATH. "/theme/images/qr1.png"; ?><!--" style="width: 5rem;">-->
<!--            <br>-->
<!--            DU'IT 海外市场部-->
<!--          </div>-->
<!--        </div>-->
        <div class="col-12 text-center">
          <div class="text-center font-23" style="display: inline-block;">
            <img src="<?php echo THEME_PATH. "/theme/images/qr2.png"; ?>" style="width: 5rem;">
            <div class="clearfix-10"></div>
            DU'IT 官方微信
          </div>
        </div>
      </div>
      <div class="clearfix-15"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 text-center">
      <div class="clearfix-15"></div>
      <div class="font-9">
        <!-- Copyright © 2017 DUIT All Rights Reserved. -->
        Orbis Australasia Pty Ltd.
      </div>
    </div>
  </div>
  <div class="clearfix-15"></div>
</footer>

<script>
    $(document).ready(function(){
        var isShow = false;
        $(".footer-modal").css({"top" : $(".wechat-icon").position().top-170, "left" : $(".wechat-icon").position().left-55});

        $(".wechat-icon").on("click", function(){
            if(!isShow){
                $(".footer-modal").fadeIn("fast");
                isShow = true;

            }else{
                $(".footer-modal").fadeOut("fast");
                isShow = false;
            }
        });
        var isShowMobile = false;
        $(".footer-modal-small").css({"top" : $(".wechat-icon-small").position().top-105, "left" : $(".wechat-icon-small").position().left-38});

        $(".wechat-icon-small").on("click", function(){
            if(!isShowMobile){
                $(".footer-modal-small").fadeIn("fast");
                isShowMobile = true;

            }else{
                $(".footer-modal-small").fadeOut("fast");
                isShowMobile = false;
            }
        });
    });
</script>

<?php wp_footer(); ?>
</body>
</html>
