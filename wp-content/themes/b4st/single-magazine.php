<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo get_template_directory_uri() . '/theme/plugins/turnjs4/extras/modernizr.2.5.3.min.js' ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/theme/plugins/turnjs4/lib/turn.js' ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/theme/plugins/turnjs4/lib/hash.js' ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/theme/plugins/turnjs4/lib/scissor.js' ?>"></script>
	<script src="<?php echo get_template_directory_uri() . '/theme/plugins/turnjs4/lib/zoom.js' ?>"></script>

  <style type="text/css">
  	body,html{
  		overflow: hidden;

  	}
  </style>
</head>
<body <?php body_class(); ?>>

<?php 
	$magazines = get_posts(array("numberposts" => -1, "post_type" => "magazine", "order" => "DESC")); 
	// var_dump($magazines);
	$front = get_field("magazine_frontcover");
	$back = get_field("magazine_backcover");
	$pages = get_field("magazine_pages");
	// var_dump($pages);
?>
<div class="overview" style="position: fixed; width: 100%; padding-top: 1rem; padding-bottom: 1rem;">
	<div style="background-color: #000; text-align: center;">
		<div><p><span class="white"><?php echo get_the_title(); ?></span>
		<span>
			<select class="magazine-select" style="width: initial; display: inline;">
				<option value="false">查看其它周刊</option>
				<?php foreach ($magazines as $magazine): ?>
					<option value="<?php echo get_permalink($magazine); ?>"><?php echo $magazine->post_title ?></option>
				<?php endforeach ?>
			</select>
		</span></p></div>
		<div class="zoom-icon zoom-icon-in"></div>
	</div>
</div>
<div id="magazine">
	<div class="magazine">
		<div class="flipbook-viewport">
			<div class="containers">
				<div id="flipbook">
					<div class="hard"><?php echo wp_get_attachment_image($front, "full") ?></div>
					<?php foreach ($pages as $page): ?>
						<div>
							<img src="<?php echo $page["url"]; ?>" alt="<?php echo $page->alt ?>">
						</div>
					<?php endforeach ?>
					<div class="hard"><?php echo wp_get_attachment_image($back, "full") ?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-buttons" style="position: fixed; bottom: 0; width:100%; background-color: #000; padding-top: 1rem; padding-bottom: 1rem; color: white; cursor: pointer; display: none;">
		<div class="home-button" style="width: 30%; text-align: center; display: inline-block;"><a href="<?php echo home_url(); ?>" style="color: white; display: inline-block;">回首页</a></div>
		<div class="next-button" style="width: 30%; text-align: center; display: inline-block;">下一页</div>
		<div class="previous-button" style="width: 30%; text-align: center; display: inline-block;">上一页</div>
	</div>
	<div class="thumbnails hidden-md-down">
		<ul class="thumbanil-list">
		<?php 
		$number = sizeof($pages) + 2;
		for ($i=1; $i <= $number; $i++) { 
			if ($i==1) {
				echo "<li class=\"$i hard\"><img src=\"" . wp_get_attachment_image_url($front, "full") . "\"></li>";
			}
			elseif ($i==$number) {
				echo "<li class=\"$i hard\"><img src=\"" . wp_get_attachment_image_url($back, "full") . "\"></li>";
			}
			else{
		?>	
			<li class="<?php echo $i; ?> double">
				<img src="<?php echo $pages[$i-2]["url"]; ?>">
				<img src="<?php echo $pages[$i-1]["url"]; ?>">
				<?php $i++; ?>
			</li>
		<?php
			}
		}
		?>
		</ul>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var $flipbook;
	var ratio = 478/595;
	var width = $(window).width();
	var height = width/ratio;
	// alert(width);
	if(width>1000){
		$(".flipbook-viewport .containers").css("width", 956);
		$flipbook = $("#flipbook").turn({
			width: 956,
			height: 595,
			autoCenter: true,
			// acceleration: true,
            duration: 1000,
		});
	}
	else{
		$flipbook = $("#flipbook").turn({
			width: width,
			height: height,
			autoCenter: true,
			display: "single",
			acceleration: true,
		});
		$(".page-buttons").show();
	}
	$(".next-button").on("click", function() {
		// alert("trigger");
		$flipbook.turn("next");
	});
	$(".previous-button").on("click", function() {
		// alert("trigger");
		$flipbook.turn("previous");
	});
	$( "select" ).change(function(){
	    var str = "";
	    $( "select option:selected" ).each(function() {
	    	str = $( this ).val();
	    });
	    if (str != "false") {
	    	window.location.href = str;
	    }
	}).change();
	$(".thumbnails li").on("click", function(){
		var turnTo = $(this).attr("class");
		// alert($turnTo);
		if(turnTo>1 && turnTo%2==1){
			turnTo -=1;
		}
		if ($('#flipbook').turn('is')) {
			$flipbook.turn("page",turnTo);
		}
	});
	// $(".zoom-icon-in").on("click", function(){
	// 	var fbdZoom = true;
	// 	if(fbdZoom) {
	// 		//$("#zoom-viewport").zoom("zoomOut");
	// 		fbdZoom = false;
	// 		$(this).addClass("zoom-icon-out").removeClass("zoom-icon-in");
	// 		$("#zoom-viewport").css("zoom", 1);
	// 		$("#flipbook").turn("display", "double");
	// 		$("#flipbook").turn("size", 840,565);
	// 		$("#flipbook").turn("resize");
	// 	} else {
	// 		//$("#zoom-viewport").zoom("zoomIn");
	// 		fbdZoom = true;
	// 		$(this).addClass("zoom-icon-out").removeClass("zoom-icon-in");
	// 		$("#zoom-viewport").css("zoom", 2);
	// 		$("#flipbook").turn("display", "double");
	// 		$("#flipbook").turn("size", 420,565);
	// 		$("#flipbook").turn("resize");
	// 	}
	// });
});
</script>
<?php wp_footer(); ?>
</body>
</html>