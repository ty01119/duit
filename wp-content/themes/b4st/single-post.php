<?php get_header(); ?>
<?php
if ( have_posts() ) :
  while ( have_posts() ) : the_post();
?>
<div id="content" role="main">
  <div id="post">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-10 mx-auto">
        <div class="breadcrumb">
          <span><a href="<?php echo home_url(); ?>">首页</a></span>&nbsp;&nbsp;&gt;
          <span><a href="<?php echo home_url(); ?>/news">资讯中心</a></span>&nbsp;&nbsp;&gt;
          <?php 
          $categories = get_the_category(); 
          // var_dump($categories);
          foreach ($categories as $category) {
            if ($category->category_blank == 9) {
              echo "<span><a href=\"" . get_category_link($category->term_id). "\">$category->name</a></span>";
            }
          }
          ?>
        </div>
        <div class="title">
          <h1 class="text-center blue"><?php the_title(); ?></h1>
          <hr class="border-blue">
        </div>
        <div class="info text-center blue">
          <span><?php echo get_the_date('Y-m-d H:i:s'); ?></span>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <span><?php echo get_field("news_source"); ?></span>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <span class="share" style="position: relative;">
            分享 &nbsp;&nbsp;
            <img id="wechat" src="<?php echo THEME_PATH."/theme/images/wechat.jpg"; ?>"  onmouseover="onMouseoverXCode()" onmouseout="onMouseoutXCode()" > 
            <a href="http://v.t.sina.com.cn/share/share.php?title=<?php echo get_the_title() ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&sourceUrl=<?php echo get_permalink(); ?>" target="_blank"><img id="weibo" src="<?php echo THEME_PATH."/theme/images/weibo.jpg"; ?>"></a>
            <a href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=<?php echo get_the_title() ?>&url=<?php echo get_permalink(); ?>" target="_blank"><img id="qz" src="<?php echo THEME_PATH."/theme/images/QZ.jpg"; ?>"></a>
            <a href="http://v.t.qq.com/share/share.php?c=share&a=index&appkey=5bd32d6f1dff4725ba40338b233ff155&title=<?php echo get_the_title() ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&url=<?php echo get_permalink();?>" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/Tencent.jpg"; ?>"></a>

            <div class="towdimcodelayer js-transition" id="layerWxcode">
              <div class="arrow js-arrow-down"></div>
              <div class="layerbd">
                <div class="codebg"><img class="xtag" src="http://qr.liantu.com/api.php?w=300&text=<?php echo get_permalink(); ?>"></div>
                <div class="codettl">打开微信扫一扫</div>
              </div>
            </div>
          </span>
        </div>
        <div class="clearfix-10"></div>
        <div class="excerpt text-center">
          <div class="col-lg-6 mx-auto">
            <h2>
              <i class="fa fa-quote-left" aria-hidden="true"></i>&nbsp;
              <?php echo str_split(get_the_excerpt(), 240)[0]; ?>&nbsp;
              <i class="fa fa-quote-right" aria-hidden="true"></i>
            </h2>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="content">
          <?php the_content(); ?>
        </div>
        <hr>
        <div class="clearfix"></div>
        <div class="recommended-post">
          <div class="wrapper">
            <div class="header text-center">
              <h2 class="blue">往期推荐</h2>
            </div>
            <div class="clearfix"></div>
            <div class="posts">
            <div class="row">
              <?php 
              $posts = get_posts(array(
                "numberposts" => 3
              ));
              ?>
              <?php foreach ($posts as $post): ?>
                <div class="col-lg-4">
                  <div class="image">
                    <div class="clearfix-10"></div>
                    <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                  </div>
                  <div class="title text-center">
                    <div class="clearfix-10"></div>
                    <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_title($post); ?></a>
                  </div>
                </div>
              <?php endforeach ?>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<script>
function dolog(_info, _callback) {
var img = new Image();
img.onload=img.onerror=_callback;
img.src = "<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png";
}

var layerWxcode = document.getElementById('layerWxcode');
function onMouseoverXCode(){
layerWxcode.className = 'towdimcodelayer js-transition js-show-up';
}
function onMouseoutXCode(){
layerWxcode.className = 'towdimcodelayer js-transition';
}
</script>
<?php
  endwhile;
endif;
?>
<?php get_footer(); ?>