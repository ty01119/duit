<?php get_header(); ?>

  <div id="content" role="main">
    <div class="row">
      <div class="col-12">
        <div class="banner">
          <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <?php $option = get_option("home_page_banner"); ?>
              <?php if(!empty($option)){ ?>
                <?php for ($i=0; $i < count($option); $i++) { ?>
                  <?php if ($i==0){ ?>
                    <div class="carousel-item active">
                      <div style="display: block;">
                      <a href="<?php echo $option[$i]["link"]; ?>"><img class="d-block img-fluid" src="<?php echo $option[$i]["image"]; ?>" alt="<?php echo $option[$i]["alt"]; ?>"></a>
                      </div>
                    </div>
                  <?php }else{ ?>
                    <div class="carousel-item">
                      <div style="display: block;">
                      <img class="d-block img-fluid" src="<?php echo $option[$i]["image"]; ?>" alt="<?php echo $option[$i]["alt"]; ?>">
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
      </div>
      </div>
    </div> <!-- end row -->
    <div class="row">
      <div class="col-12 anchor">
        <div class="row">
          <div class="col-10 offset-1">
            <div class="clearfix" style="height: 2.33rem;"></div>
            <div class="text-center">
              <a href="<?php echo home_url(); ?>/news/"><img src="<?php echo THEME_PATH."/theme/images/t1.png"; ?>" style="max-width: 100%;"></a>
            </div>
            <hr class="blue background-blue border-blue">
            <!-- Tab container -->
            <div class="clearfix-15"></div>
            <div class="tab-container">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo home_url() ?>/news/?page=skincare">护肤小贴士</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo home_url() ?>/news/?page=childcare">母婴育儿</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo home_url() ?>/news/?page=advise">专家建议</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="<?php echo home_url() ?>/news/?page=latest">最新动态</a>
              </li>
            </ul>
            </div> <!-- end tab container -->
            <div class="clearfix"></div>
            <!-- Tab panels -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="skincare">
                <div class="row">
                  <?php 
                  $args = array( 'numberposts' => 3, 'post_type' => 'post' ); 
                  $posts = get_posts($args);
                  ?>
                  <?php foreach ($posts as $post): ?>
                    <div class="col-lg-4">
                      <div class="single-post">
                        <div class="wrapper">
                          <div class="image">
                            <a href="<?php echo get_permalink($post); ?>"><img src="<?php echo get_the_post_thumbnail_url($post, "full"); ?>"></a>
                          </div>
                          <div class="title text-center">
                            <p><a href="<?php echo get_permalink($post); ?>"><?php echo $post->post_title; ?></a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach ?>
                </div>
              </div>
            </div> <!-- end tab panels -->
          </div>
          <div class="magazine hidden-md-down mr-auto">
            <div class="wrapper">
              <?php $magazine = get_posts(array('numberposts' => 1, 'post_type' => 'magazine')); ?>
              <a href="<?php echo get_permalink($magazine[0]) ?>" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/i1.png" ;?>"></a>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- end row -->
  </div><!-- /#content -->
  <script type="text/javascript">
    (function ($) {
      $(document).ready(function(){
        'use strict';
        $(".magazine .wrapper").pin({
          // containerSelector: ".anchor",
          containerSelector: ".magazine",
          padding: {top: 183}
        });
      });
    })(jQuery);
  </script>

<?php get_footer(); ?>
