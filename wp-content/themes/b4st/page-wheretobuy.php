<?php get_header(); ?>

<div id="content" role="main">
  <div class="row">
    <div class="col-12">
      <div class="banner">
        <?php $banner = get_option('wheretobuy_page_banner'); ?>
        <?php if (!empty($banner)): ?>
          <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['alt'] ?>">
        <?php else: ?>
          <img src="<?php echo THEME_PATH."/theme/images/about-banner.jpg"; ?>">
        <?php endif ?>
      </div>
    </div>
  </div> <!-- end row -->
  <div class="row">
    <div class="col-10 mx-auto">
      <div class="clearfix" style="height: 2.33rem;"></div>
      <div class="text-center">
        <img src="<?php echo THEME_PATH."/theme/images/wheretobuy-text.jpg"; ?>" style="max-width: 100%;">
      </div>
      <hr class="blue background-blue border-blue">
      <div class="clearfix"></div>
      <div class="brands">
        <div class="brand">
          <div class="image">
            <a href="http://mall.jd.hk/index-216697.html" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/jd.jpg"; ?>"></a>
          </div>
          <div class="clearfix"></div>
          <div class="text">
            <a href="http://mall.jd.hk/index-216697.html" target="_blank">DU'IT全球购旗舰店</a>
          </div>
        </div>
        <div class="brand">
          <div class="image">
            <a href="https://duit.tmall.hk/" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/tmall.jpg"; ?>"></a>
          </div>
          <div class="clearfix"></div>
          <div class="text">
            <a href="https://duit.tmall.hk/" target="_blank">DU'IT天猫旗舰店</a>
          </div>
        </div>
        <div class="brand">
          <div class="image">
            <a href="http://list.vip.com/1421878.html?ff=72%7C1%7C1%7C1" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/wph.jpg"; ?>"></a>
          </div>
          <div class="clearfix"></div>
          <div class="text">
            <a href="http://list.vip.com/1421878.html?ff=72%7C1%7C1%7C1" target="_blank">唯品会</a>
          </div>
        </div>
        <div class="brand">
          <div class="image">
            <a href="https://chemistwarehouse.tmall.hk/?q=duit&type=p&search=y&newHeader_b=s_from&searcy_type=item&from=.shop.pc_2_searchbutton&spm=a1z10.3-b-s.a2227oh.d101" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/webeat.jpg"; ?>"></a>
          </div>
          <div class="clearfix"></div>
          <div class="text">
            <a href="https://chemistwarehouse.tmall.hk/?q=duit&type=p&search=y&newHeader_b=s_from&searcy_type=item&from=.shop.pc_2_searchbutton&spm=a1z10.3-b-s.a2227oh.d101" target="_blank">Chemist Warehouse天猫旗舰店</a>
          </div>
        </div>
        <div class="brand">
          <div class="image">
            <a href="https://auspost.tmall.hk/?q=duit&type=p&search=y&newHeader_b=s_from&searcy_type=item&from=.shop.pc_2_searchbutton&spm=a1z10.3-b-s.a2227oh.d101" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/post.jpg"; ?>"></a>
          </div>
          <div class="clearfix"></div>
          <div class="text">
            <a href="https://auspost.tmall.hk/?q=duit&type=p&search=y&newHeader_b=s_from&searcy_type=item&from=.shop.pc_2_searchbutton&spm=a1z10.3-b-s.a2227oh.d101" target="_blank">澳洲邮政天猫旗舰店</a>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- end row -->
</div>
<div class="clearfix"></div>
<?php get_footer(); ?>