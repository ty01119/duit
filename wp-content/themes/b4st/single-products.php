<?php get_header(); ?>
<?php
if ( have_posts() ) :
  while ( have_posts() ) : the_post();
    // Your loop code

$comments = get_field("product_comments"); 
$commentsCount = count($comments);

$star1Count = 0;
$star2Count = 0;
$star3Count = 0;
$star4Count = 0;
$star5Count = 0;

$star1Comment  = array();
$star2Comment  = array();
$star3Comment  = array();
$star4Comment  = array();
$star5Comment  = array();

$total = 0;
if ($comments) {
  foreach ($comments as $comment) {
    $total += $comment["score"];
    switch ($comment['score']) {
      case "1":
        $star1Count ++;
        array_push($star1Comment, $comment);
        break;
      case "2":
        $star2Count ++;
        array_push($star2Comment, $comment);
        break;
      case "3":
        $star3Count ++;
        array_push($star3Comment, $comment);
        break;
      case "4":
        $star4Count ++;
        array_push($star4Comment, $comment);
        break;
      case "5":
        $star5Count ++;
        array_push($star5Comment, $comment);
        break;
    }
  }
  $score = $total/$commentsCount;
}
$stars5 = intval(get_field("5stars"));
$stars4 = intval(get_field("4stars"));
$stars3 = intval(get_field("3stars"));
$stars2 = intval(get_field("2stars"));
$stars1 = intval(get_field("1stars"));
$starTotal = $stars5 + $stars4 + $stars3 + $stars2 + $stars1;
$totalScore = 5*$stars5 + 4*$stars4 + 3*$stars3 + 2*$stars2 + $stars1;
if ($totalScore) {
  $showStars = $totalScore/$starTotal;
}
?>
<div id="content" role="main">
  <div id="products">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-10 mx-auto">
        <div class="breadcrumb">
          <span><a href="<?php echo home_url(); ?>">首页</a></span>&nbsp;&nbsp;&gt;
          <span><a href="<?php echo home_url(); ?>/products">全线产品</a></span>&nbsp;&nbsp;&gt;
          <?php 
          $categories = get_the_category(); 
          foreach ($categories as $category) {
            if ($category->category_parent == 3) {
              echo "<span><a href=\"" . get_category_link($category->term_id). "\">$category->name</a></span>";
            }
          }
          ?>
        </div>
        <div class="clearfix"></div>
        <div class="detail">
          <div class="row" style="min-height: 35rem;">
            <div class="col-lg-5 hidden-md-down">
              <div class="bzoom-wrap">
              <ul id="bzoom">
                <?php 
                $photos = get_field("product_galary"); 
                foreach ($photos as $photo) {
                  echo "<li>";
                  echo wp_get_attachment_image($photo['product_image'], 'full',false, array('class' => 'bzoom_thumb_image'));
                  echo wp_get_attachment_image($photo['product_image'], 'full',false, array('class' => 'bzoom_big_image'));
                  echo "</li>";
                }
                ?>
              </ul>
              </div>
            </div>
            <div class="col hidden-lg-up">
              <div class="mobile-thumbnail-image">
                <img src="<?php echo wp_get_attachment_image_url($photos[0]['product_image'], 'full'); ?>">
              </div>
              <div class="clearfix-10"></div>
              <ul class="mobile-thumbnails">
              <?php foreach ($photos as $photo): ?>
                <li class="mobile-thumbnail"><?php echo wp_get_attachment_image($photo['product_image'], 'full'); ?></li>
              <?php endforeach ?>
              </ul>
            </div>
            <div class="col-lg-7">
                <div class="content">
                  <div class="title">
                    <h2><?php the_title(); ?></h2>
                  </div>
                  <div class="subtitle">
                    <h2 class="light"><?php echo get_field("product_subtitle"); ?></h2>
                  </div>
                  <div class="clearfix-15"></div>
                  <div class="excerpt">
                    <p class="blue"><?php echo get_the_excerpt(); ?></p>
                  </div>
                  <div class="clearfix"></div>
                  <div class="tag-fields">
                    <div class="row">
                      <div class="col-md-4">
                        <p><b>品牌： <?php echo get_field("product_brand"); ?></b></p>
                        <p><b>产地： <?php echo get_field("product_origin"); ?></b></p>
                        <p><b>净含量： <?php echo get_field("product_weight"); ?></b></p>
                        <?php 
                        $categories = get_the_category(); 
                        foreach ($categories as $category) {
                          if ($category->category_parent == 3) {
                            echo "<p><b>分类： $category->name</b></p>";
                          }
                        }
                        ?>
                        <p><b>编号： <?php echo get_field("product_sn"); ?></b></p>
                      </div>
                      <div class="col-md-8">
                        <div style="position: relative; display: inline-block;">
                          <div style="display: inline-block;">
                          <b>
                            推荐指数： 
                            <?php for($i=0; $i<intval(ceil($showStars)); $i++){ ?>
                            <span><i class="fa fa-star"></i></span>
                            <?php } ?>
                          </b>
                          </div>
                          <div style="position: relative; display: inline-block; margin-left: .5rem;">
                            <i class="fa fa-info-circle hover" aria-hidden="true"></i>
                          </div>
                          <div class="recommended-block bold">
                              <div class="wrapper">
                                <p>对我的印象，怎么样？<img src="<?php echo THEME_PATH. "/theme/images/recommended-block-close.png"; ?>" class="recommended-block-close" style="float: right;"></p>
                                <hr>
                                <div class="scores">
                                  <div class="score">
                                    <div class="text">
                                      非常好
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: <?php echo $stars5/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-number">(<?php echo $stars5; ?>)</div>
                                  </div>
                                  <div class="score">
                                    <div class="text">
                                      好
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: <?php echo $stars4/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-number">(<?php echo $stars4; ?>)</div>
                                  </div>
                                  <div class="score">
                                    <div class="text">
                                      一般
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: <?php echo $stars3/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-number">(<?php echo $stars3; ?>)</div>
                                  </div>
                                  <div class="score">
                                    <div class="text">
                                      不好
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: <?php echo $stars2/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-number">(<?php echo $stars2; ?>)</div>
                                  </div>
                                  <div class="score">
                                    <div class="text">
                                      非常不好
                                    </div>
                                    <div class="progress">
                                      <div class="progress-bar" role="progressbar" style="width: <?php echo $stars1/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="text-number">(<?php echo $stars1; ?>)</div>
                                  </div>
                                  <div class="clearfix-10"></div>
                                  <div class="font-12" style="‑webkit‑text‑size‑adjust: none;">
                                      <img src="<?php echo THEME_PATH . "/theme/images/recommended-block-text.png" ?>" style="width: 100%;">
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                        <div class="towdimcodelayer js-transition" id="layerWxcode">
                          <div class="arrow js-arrow-down"></div>
                          <div class="layerbd">
                            <div class="codebg"><img class="xtag" src="http://qr.liantu.com/api.php?w=300&text=<?php echo get_permalink(); ?>"></div>
                            <div class="codettl">打开微信扫一扫</div>
                          </div>
                        </div>
                        <p class="share"><b>
                          分享到： &nbsp;&nbsp;
                          <img id="wechat" src="<?php echo THEME_PATH."/theme/images/wechat.jpg"; ?>"  onclick="onMouseoverXCode()" >
                          <a href="http://v.t.sina.com.cn/share/share.php?title=<?php echo get_the_title() ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&sourceUrl=<?php echo get_permalink(); ?>" target="_blank"><img id="weibo" src="<?php echo THEME_PATH."/theme/images/weibo.jpg"; ?>"></a>
                          <a href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=<?php echo get_the_title() ?>&url=<?php echo get_permalink(); ?>" target="_blank"><img id="qz" src="<?php echo THEME_PATH."/theme/images/QZ.jpg"; ?>"></a>
                          <a href="http://v.t.qq.com/share/share.php?c=share&a=index&appkey=5bd32d6f1dff4725ba40338b233ff155&title=<?php echo get_the_title() ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&url=<?php echo get_permalink();?>" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/Tencent.jpg"; ?>"></a>
                        </b></p>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="where-to-buy">
                    <div class="row">
                      <div class="col-md-2">
                        <p><b>购买链接：</b></p>
                      </div>
                      <div class="col-md-10">
                        <div class="link-logo">
                        <?php $links = get_field("product_links");
                          foreach ($links as $link) {
                            echo "<a href=\"" . $link["product_link"] . "\" target=\"_blank\">" .wp_get_attachment_image($link['product_image'], 'full') . "</a>";
                          }
                        ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="bottom-panel">
          <div class="row">
            <div class="col-lg-2 sidebar hidden-md-down">
              <?php 
              $index = 0;
              $current = 0;
              $productsCategory = get_category_by_slug("products");
              $categories=get_categories(
                  array( 'parent' => $productsCategory->term_id )
              );
              // var_dump($categories);
              ?>
              <!-- accordion begin -->
              <div id="accordion">
                <?php 
                foreach ($categories as $category){
                  if ($category->name == single_cat_title('', false)) {
                    $current = $index;
                  }
                  $index++;
                  echo "<h2>$category->name</h2>";
                  echo  "<div>" ;
                  $products = get_posts(
                    array(
                      'numberposts' => -1, 
                      'category' => $category->term_id,
                      'post_type' => "products",
                    )
                  );
                  foreach ($products as $product) {
                    echo "<p class=\"font-18\"><a href=\"" . get_permalink($product). "\">$product->post_title</a></p>";
                  }
                  echo "</div>";
                }
                ?>
              </div> <!-- accordion end -->
              <div class="clearfix"></div>
              <?php 
              $recommendedProducts = get_posts(array(
                  'numberposts' => -1,
                  'post_type' => 'products',
                  'meta_key' => 'product_recommended',
                  'meta_value' => true
                ));
              // var_dump($recommendedProducts);
              ?>
              <div id="recommended">
                <div class="recommended-img text-center">
                  <img src="<?php echo THEME_PATH ?>/theme/images/recommended.png">
                </div>
                <div class="clearfix-15"></div>
                <div class="recommended">
                  <ul style="margin: 0px; padding: 0px; position: relative; list-style: none;">
                    <?php foreach ($recommendedProducts as $recommended): ?>
                      <li class="single-recommended">
                        <div class="image">
                          <a href="<?php echo get_permalink($recommended); ?>"><?php echo get_the_post_thumbnail($recommended, "full"); ?></a>
                        </div>
                        <div class="clearfix-5"></div>
                        <div class="title text-center"><a href="<?php echo get_permalink($recommended); ?>"><?php echo $recommended->post_title; ?></a></div>
                        <div class="clearfix-5"></div>
                      </li>
                    <?php endforeach ?>
                  </ul>
                </div>
                <div class="navi font-30 text-center">
                  <a href="#" class="prev"><i class="fa fa-caret-up blue"></i></a>
                  &nbsp;&nbsp;&nbsp;
                  <a href="#" class="next"><i class="fa fa-caret-down blue"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="clearfix-15"></div>
              <div id="info-accordion">
                <h2 class="blue">产品介绍<hr class="border-blue"></h2>
                <div class="blue">
                  <?php echo get_field("product_intro"); ?>
                </div>
                <h2 class="blue">产品功效<hr class="border-trans"></h2>
                <div class="blue">
                  <?php echo get_field("product_effect"); ?>
                </div>
                <h2 class="blue">产品成分<hr class="border-trans"></h2>
                <div class="blue">
                  <?php echo get_field("product_ingredient"); ?>
                </div>
                <h2 class="blue">使用方法<hr class="border-trans"></h2>
                <div class="blue">
                  <?php echo get_field("product_instruction"); ?>
                </div>
                <h2 class="blue">宝贝展示<hr class="border-trans"></h2>
                <div class="blue images">
                  <?php 
                  $images =  get_field("product_reveal_repeater"); 
                  foreach ($images as $image) {
                    echo "<div class=\"image\">";
                    echo wp_get_attachment_image($image['product_reveal'], 'full');
                    echo "</div>";
                  }
                  ?>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="comments">
                <h2 class="blue">用户点评<hr class="border-blue"></h2>
                <p><b>
                  推荐指数： 
                  <?php for($i=0; $i<intval(ceil($showStars)); $i++){ ?>
                  <span><i class="fa fa-star"></i></span>
                  <?php } ?>
                  &nbsp;&nbsp;
                  <?php echo number_format($showStars, 1); ?>
                  &nbsp;&nbsp; | &nbsp;&nbsp;
                  共有<?php echo $commentsCount; ?>个产品点评
                </b></p>
                <div class="clearfix-15"></div>
                <!-- comment panel -->
                <div class="comment-panel border border-grey">
                  <!-- Tab container -->
                  <div class="tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" href="#all" role="tab" data-toggle="tab">全部<?php echo $commentsCount; ?></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#star5" role="tab" data-toggle="tab">5星</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#star4" role="tab" data-toggle="tab">4星</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#star3" role="tab" data-toggle="tab">3星</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#star2" role="tab" data-toggle="tab">2星</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#star1" role="tab" data-toggle="tab">1星</a>
                      </li>
                    </ul>
                  </div> <!-- end tab container -->
                  <div class="clearfix"></div>
                  <!-- Tab content -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="all">
                      <?php 
                      for ($i=0; $i < $commentsCount; $i++) { 
                        $comment = $comments[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="wrapper">
                            <div class="user">
                              用户：<?php echo $comment['user']; ?>
                            </div>
                            <div class="source">
                              来源：<?php echo $comment['source']; ?>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="star5">
                      <?php 
                      for ($i=0; $i < $star5Count; $i++) { 
                        $comment = $star5Comment[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="user">
                            用户：<?php echo $comment['user']; ?>
                          </div>
                          <div class="source">
                            来源：<?php echo $comment['source']; ?>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="star4">
                      <?php 
                      for ($i=0; $i < $star4Count; $i++) { 
                        $comment = $star4Comment[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="user">
                            用户：<?php echo $comment['user']; ?>
                          </div>
                          <div class="source">
                            来源：<?php echo $comment['source']; ?>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="star3">
                      <?php 
                      for ($i=0; $i < $star3Count; $i++) { 
                        $comment = $star3Comment[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="user">
                            用户：<?php echo $comment['user']; ?>
                          </div>
                          <div class="source">
                            来源：<?php echo $comment['source']; ?>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="star2">
                      <?php 
                      for ($i=0; $i < $star2Count; $i++) { 
                        $comment = $star2Comment[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="user">
                            用户：<?php echo $comment['user']; ?>
                          </div>
                          <div class="source">
                            来源：<?php echo $comment['source']; ?>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="star1">
                      <?php 
                      for ($i=0; $i < $star1Count; $i++) { 
                        $comment = $star1Comment[$i];
                      ?>
                        <div class="single-comment">
                          <div class="date">
                            <?php echo $comment['date']; ?>
                          </div>
                          <div class="comment">
                            <?php echo $comment['comment']; ?>
                          </div>
                          <div class="user">
                            用户：<?php echo $comment['user']; ?>
                          </div>
                          <div class="source">
                            来源：<?php echo $comment['source']; ?>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div> <!-- end tab content -->
                </div> <!-- end comment panel -->
                <div class="clearfix-15"></div>
                <div class="font-12">* 以上点评数据来源来自第三方网站</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  if ($(window).width()>1366) {
    $("#bzoom").zoom({
        thumb_image_width: 550,
        thumb_image_height: 375,
        source_image_width: 1100,
        source_image_height: 760,
        zoom_area_width: 550,
        zoom_area_height: 375,
        autoplay_interval :3000,
        small_thumbs : 4,
        autoplay : false
    });
  }
  $(function() {
    var icons = {
      header: "fa fa-caret-right",
      activeHeader: "fa fa-caret-down"
    };
    $( "#accordion" ).accordion({
      collapsible: true,
      icons: icons,
      heightStyle: "content",
      active: <?php echo $current ?>,
    });
  });

  $(function() {
    var icons = {
      header: "fa fa-plus",
      activeHeader: "fa fa-minus"
    };

    $( "#info-accordion" ).accordion({
      collapsible: true,
      icons: icons,
      heightStyle: "content",
      active: 4,
      beforeActivate: function(event, ui) {
        if (ui.newHeader[0]) {
          var currHeader  = ui.newHeader;
          var currContent = currHeader.next('.ui-accordion-content');
        } else {
          var currHeader  = ui.oldHeader;
          var currContent = currHeader.next('.ui-accordion-content');
        }
        var isPanelSelected = currHeader.attr('aria-selected') == 'true';

        currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

        currContent.toggleClass('accordion-content-active',!isPanelSelected)    
        if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        return false;
        }
    });

    $( "#info-accordion" ).accordion({
      icons: icons,
      heightStyle: "content",
      active: 0,
    });
  });
(function($) {
  $(function() {
    $("#info-accordion").accordion({ header: "h3", collapsible: true });
  })
})(jQuery);
$(window).load(function() {
    $("#recommended .recommended").jCarouselLite({
      btnNext: "#recommended .next",
      btnPrev: "#recommended .prev",
      vertical: true
    });
});
  $(".mobile-thumbnail").on("click", function(){
    $(this).parent().parent().find(".mobile-thumbnail-image img").attr("src",$(this).find("img").attr("src"))
  });
  var recommendedShow = false;
  $(".recommended-block").css({"top" : "-.5rem", "right" : "-11.5rem"});

  $(".hover").on("click", function(event){
    if(!recommendedShow){
        $(".recommended-block").fadeIn("fast");
        recommendedShow = true;
    }else{
        $(".recommended-block").fadeOut("fast");
        recommendedShow = false;
    }
  });
  $(".recommended-block-close").on("click", function(){
      $(".recommended-block").fadeOut("fast");
      recommendedShow = false;
  });
});
</script>
<script>
function dolog(_info, _callback) {
var img = new Image();
img.onload=img.onerror=_callback;
img.src = "<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png";
}
var wxShow = false;
var layerWxcode = document.getElementById('layerWxcode');
function onMouseoverXCode(){
    if(!wxShow){
        layerWxcode.className = 'towdimcodelayer js-transition js-show-up';
        wxShow = true;
    }else{
        layerWxcode.className = 'towdimcodelayer js-transition';
        wxShow = false;
    }
}

</script>
<?php
  endwhile;
endif;
?>
<?php get_footer(); ?>
