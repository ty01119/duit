<?php get_header(); global $wp_query;?>
<?php 
if ( have_posts() ) { 
  $news = array();
  $products = array();
  $counter = 0;
  while ( have_posts() ) { 
    $counter ++;
    the_post();
    $thePost = get_post(); 
    if ($thePost->post_type == "products") {
      array_push($products, $thePost);
    }
    elseif ($thePost->post_type == "post") {
      array_push($news, $thePost);
    }
  }
  $productCount = count($products);
  $newsCount = count($news);
  $total = $productCount + $newsCount;
  // var_dump($newsCount);
}
?>
<div id="content" role="main">
  <div id="category" class="news-list">
    <div class="row">
      <div class="col-10 mx-auto">
        <div style="margin-top: 1rem;"></div>
        <?php if ($total): ?>
          
        <div class="product-category">
          <h2 class="blue text-center">找到&quot;<?php the_search_query(); ?>&quot;相关信息<?php echo $total; ?>条</h2>
          <div class="clearfix"></div>
        </div>

        <!-- Tab container -->
        <div class="tab-container">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" href="#products" role="tab" data-toggle="tab">产品</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#news" role="tab" data-toggle="tab">资讯</a>
            </li>
          </ul>
        </div> <!-- end tab container -->
          <!-- Tab content -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="news">
            <?php foreach ($news as $post): ?>
            <div class="post">
              <div class="single-post">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="wrapper">
                      <div class="image">
                        <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="info">
                      <div class="title">
                        <h2><?php echo $post->post_title; ?></h2>
                      </div>
                      <div class="clearfix-15"></div>
                      <div class="date">
                        <h5><?php echo get_the_date('Y-m-j', $post); ?></h5>
                      </div>
                      <div class="clearfix"></div>
                      <div class="excerpt">
                        <p><?php echo get_the_excerpt($post); ?></p>
                      </div>
                      <div class="clearfix"></div>
                      <div class="clearfix"></div>
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="tags">
                          <b>标签：</b>
                            <?php 
                            $tags = get_the_tags($post->ID);
                            if ($tags){
                              $index = 0; 
                              $count = count($tags);
                              foreach ($tags as $tag) {
                                $index ++;
                                if ($index < $count) {
                                  echo "<b>$tag->name, </b>";
                                }
                                else{
                                  echo "<b>$tag->name</b>";
                                }
                              }
                            } 
                            ?>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="category">
                          <b>分类：</b>
                            <?php 
                            $categories = get_the_category($post->ID);
                            $index = 0;
                            $count = count($categories);
                            if ($categories) {
                              foreach ($categories as $category) {
                                if ($category->category_blank==9) {
                                  echo "<b>$category->name</b>";
                                }
                              }
                            }
                            ?>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="readmore">
                        <a href="<?php echo get_permalink($post); ?>">查看详情</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach ?>
            <?php if (empty($news)): ?>
            <div class="post">
              <div class="single-post">
                <div class="row">
                  <div class="col-12">
                    <div class="no-result">没有相关新闻</div>
                  </div>
                </div>
              </div>
            </div>
            <?php endif ?>
          </div>

          <div role="tabpanel" class="tab-pane active" id="products">
            <?php foreach ($products as $post): ?>
              <?php
              $comments = get_field("product_comments",$post); 
              $commentsCount = count($comments);

              $star1Count = 0;
              $star2Count = 0;
              $star3Count = 0;
              $star4Count = 0;
              $star5Count = 0;

              $star1Comment  = array();
              $star2Comment  = array();
              $star3Comment  = array();
              $star4Comment  = array();
              $star5Comment  = array();

              $total = 0;
              if ($commnets) {
                foreach ($comments as $comment) {
                  $total += $comment["score"];
                  switch ($comment['score']) {
                    case "1":
                      $star1Count ++;
                      array_push($star1Comment, $comment);
                      break;
                    case "2":
                      $star2Count ++;
                      array_push($star2Comment, $comment);
                      break;
                    case "3":
                      $star3Count ++;
                      array_push($star3Comment, $comment);
                      break;
                    case "4":
                      $star4Count ++;
                      array_push($star4Comment, $comment);
                      break;
                    case "5":
                      $star5Count ++;
                      array_push($star5Comment, $comment);
                      break;
                  }
                }
                $score = $total/$commentsCount;
              }

              $stars5 = intval(get_field("5stars",$post));
              $stars4 = intval(get_field("4stars",$post));
              $stars3 = intval(get_field("3stars",$post));
              $stars2 = intval(get_field("2stars",$post));
              $stars1 = intval(get_field("1stars",$post));
              $starTotal = $stars5 + $stars4 + $stars3 + $stars2 + $stars1;
              $totalScore = 5*$stars5 + 4*$stars4 + 3*$stars3 + 2*$stars2 + $stars1;
              if ($totalScore) {
                $showStars = $totalScore/$starTotal;
              }
              ?>
            <div class="post">
              <div class="single-post">
                <div class="row">
                  <div class="col-lg-6">
                      <div class="wrapper">
                        <div class="image">
                          <a href="<?php echo get_permalink($post); ?>"><?php echo get_the_post_thumbnail($post, "full"); ?></a>
                        </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="content">
                      <div class="title">
                        <h2><?php echo get_the_title($post); ?></h2>
                      </div>
                      <div class="subtitle">
                        <h2 class="light"><?php echo get_field("product_subtitle"); ?></h2>
                      </div>
                      <div class="clearfix-15"></div>
                      <div class="excerpt">
                        <p class="blue"><?php echo get_the_excerpt($post); ?></p>
                      </div>
                      <div class="clearfix"></div>
                      <div class="tag-fields">
                        <div class="row">
                          <div class="col-md-6">
                            <p><b>品牌： <?php echo get_field("product_brand",$post); ?></b></p>
                            <p><b>产地： <?php echo get_field("product_origin",$post); ?></b></p>
                            <p><b>净含量： <?php echo get_field("product_weight",$post); ?></b></p>
                            <?php 
                            $categories = get_the_category($post->ID); 
                            foreach ($categories as $category) {
                              if ($category->category_blank == 3) {
                                echo "<p><b>分类： $category->name</b></p>";
                              }
                            }
                            ?>
                            <p><b>编号： <?php echo get_field("product_sn",$post); ?></b></p>
                          </div>

                          <div class="col-md-6">
                            <div style="position: relative; display: inline-block;">
                              <div style="display: inline-block;">
                              <b>
                                推荐指数： 
                                <?php for($i=0; $i<intval(ceil($showStars)); $i++){ ?>
                                <span><i class="fa fa-star"></i></span>
                                <?php } ?>
                              </b>
                              </div>
                              <div style="position: relative; display: inline-block; margin-left: .5rem;">
                                <i class="fa fa-info-circle hover" aria-hidden="true"></i>
                              </div>
                              <div class="recommended-block">
                                  <div class="wrapper">
                                    <h5>对我的印象，怎么样？<img src="<?php echo THEME_PATH . "/theme/images/recommended-block-close.png"; ?>" class="recommended-block-close" style="float: right;"></h5>
                                    <hr>
                                    <div class="scores">

                                      <div class="score">
                                        <div class="text">
                                          非常好
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?php echo $stars5/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="text-number">(<?php echo $stars5; ?>)</div>
                                      </div>

                                      <div class="score">
                                        <div class="text">
                                          好
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?php echo $stars4/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="text-number">(<?php echo $stars4; ?>)</div>
                                      </div>

                                      <div class="score">
                                        <div class="text">
                                          一般
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?php echo $stars3/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="text-number">(<?php echo $stars3; ?>)</div>
                                      </div>

                                      <div class="score">
                                        <div class="text">
                                          不好
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?php echo $stars2/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="text-number">(<?php echo $stars2; ?>)</div>
                                      </div>

                                      <div class="score">
                                        <div class="text">
                                          非常不好
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?php echo $stars1/$starTotal*100; ?>%" aria-valuenow="<?php echo $stars5/$starTotal*100; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="text-number">(<?php echo $stars1; ?>)</div>
                                      </div>

                                        <div class="clearfix-10"></div>
                                        <div class="font-12" style="‑webkit‑text‑size‑adjust: none;">
                                            <img src="<?php echo THEME_PATH . "/theme/images/recommended-block-text.png" ?>" style="width: 100%;">
                                        </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            <div class="towdimcodelayer js-transition" id="layerWxcode">
                              <div class="arrow js-arrow-down"></div>
                              <div class="layerbd">
                                <div class="codebg"><img class="xtag" src="http://qr.liantu.com/api.php?w=300&text=<?php echo get_permalink($post); ?>"></div>
                                <div class="codettl">打开微信扫一扫</div>
                              </div>
                            </div>
                            <div class="clearfix" style="height: 3.5rem;"></div>
                            <p class="share"><b>
                              分享到： &nbsp;&nbsp;
                              <img id="wechat" src="<?php echo THEME_PATH."/theme/images/wechat.jpg"; ?>"  onclick="onMouseoverXCode()" onmouseout="onMouseoutXCode()" >
                              <a href="http://v.t.sina.com.cn/share/share.php?title=<?php echo get_the_title() ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&sourceUrl=<?php echo get_permalink($post); ?>" target="_blank"><img id="weibo" src="<?php echo THEME_PATH."/theme/images/weibo.jpg"; ?>"></a>
                              <a href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=<?php echo get_the_title($post) ?>&url=<?php echo get_permalink($post); ?>" target="_blank"><img id="qz" src="<?php echo THEME_PATH."/theme/images/QZ.jpg"; ?>"></a>
                              <a href="http://v.t.qq.com/share/share.php?c=share&a=index&appkey=5bd32d6f1dff4725ba40338b233ff155&title=<?php echo get_the_title($post) ?>&pic=<?php echo home_url(); ?>/wp-content/themes/b4st/theme/images/logo.png&url=<?php echo get_permalink($post);?>" target="_blank"><img src="<?php echo THEME_PATH."/theme/images/Tencent.jpg"; ?>"></a>
                            </b></p>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="where-to-buy">
                        <div class="row">
                          <div class="col-md-2">
                            <p><b>购买链接：</b></p>
                          </div>
                          <div class="col-md-10 link-logo">
                            <?php $links = get_field("product_links",$post);
                              foreach ($links as $link) {
                                echo "<a href=\"" . $link["product_link"] . "\">" .wp_get_attachment_image($link['product_image'], 'full') . "</a>";
                              }
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach ?>
            <?php if (empty($products)): ?>
            <div class="post">
              <div class="single-post">
                <div class="row">
                  <div class="col-12">
                    <div class="no-result">没有相关产品</div>
                  </div>
                </div>
              </div>
            </div>
            <?php endif ?>
          </div>
        </div> <!-- end tab content -->
        <?php else: ?>
        <div class="product-category">
          <h2 class="blue text-center">未找到&quot;<?php the_search_query(); ?>&quot;相关信息</h2>
          <div class="clearfix"></div>
        </div>
        <?php endif ?>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<script type="text/javascript">
  var recommendedShow = false;
  $(".hover").on("click", function(){
    if(!recommendedShow){
        $(this).parent().parent().find(".recommended-block").css({"top" : "-.5rem", "right" : "-11.5rem"});

        $(this).parent().parent().find(".recommended-block").fadeIn("fast");
        recommendedShow = true;
    }else{
        $(this).parent().parent().find(".recommended-block").fadeOut("fast");
        recommendedShow = false;
    }
  });
  $(".recommended-block-close").on("click", function(){
      $(".recommended-block").fadeOut("fast");
      recommendedShow = false;
  });
</script>

<?php get_footer(); ?>
